#ifndef __GDF_VAL_H__
#define __GDF_VAL_H__

#include "gdf-gdb-commander.h"

void gdf_new_locals (GdfGdbCommander *cmdr);
void gdf_update_locals (GdfGdbCommander *cmdr);

#endif
