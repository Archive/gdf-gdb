/* Output generation routines for GdfGdbCommander 
 */

#ifndef __GDF_OUT_H__
#define __GDF_OUT_H__

#include "../gdb/gdb/defs.h"
#include "../gdb/gdb/ui-out.h"
#include "gdf-gdb-commander.h"

struct ui_out *gdf_out_new (GdfGdbCommander *cmdr);

#endif
