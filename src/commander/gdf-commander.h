#ifndef GDF_COMMANDER_H
#define GDF_COMMANDER_H

#include "../gdb/gdb/defs.h"

extern void gdf_command_loop (void);
extern void gdf_commander_initialize (void);

#endif
