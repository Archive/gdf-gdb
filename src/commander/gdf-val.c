#include "gdf-gdb-commander.h"
#include "gdf-val.h"

#define INITIAL_BUF_SIZE 20

static char *val_buf = NULL;
static int val_buf_len = 0;

static void fill_locals (GdfGdbCommander *cmdr, GDF_SymbolSet *s);
static void fill_value_sym (GdfGdbCommander *cmdr, GDF_Symbol *gdf_sym, 
							struct symbol *sym, struct frame_info *frame);
static void gdf_ui_file_write (struct ui_file *stream,
							   const char *buf, long buf_len);

void
gdf_new_locals (GdfGdbCommander *cmdr)
{
  CORBA_any *event_any;
  GDF_SymbolSet *ret;
  ret = GDF_SymbolSet__alloc ();
  ret->_length = 0;
  ret->_maximum = 10;
  ret->_buffer = (GDF_Symbol*)CORBA_octet_allocbuf (sizeof (GDF_Symbol)* 10);
  CORBA_sequence_set_release (ret, CORBA_TRUE);

  if (cmdr->locals)
	  CORBA_free (cmdr->locals);

  fill_locals (cmdr, ret);

  cmdr->locals = ret;

  event_any = gdf_marshal_event_long ("symbol_set_changed", 
									  GDF_SYMBOL_SET_LOCALS);
  gdf_event_channel_client_push (cmdr->event_channel, event_any);  
  CORBA_free (event_any);
}

void
gdf_update_locals (GdfGdbCommander *cmdr)
{
  int i;

  GDF_SymbolSet *ret;
  GDF_SymbolSet *old_locals;
  ret = GDF_SymbolSet__alloc ();
  ret->_length = 0;
  ret->_maximum = 10;
  ret->_buffer = (GDF_Symbol*)CORBA_octet_allocbuf (sizeof (GDF_Symbol)* 10);
  CORBA_sequence_set_release (ret, CORBA_TRUE);

  old_locals = cmdr->locals;
  fill_locals (cmdr, ret);
  cmdr->locals = ret;
  
  g_assert (ret->_length == old_locals->_length);
  
  for (i = 0; i < ret->_length; i++) 
	{
	  if (strcmp (ret->_buffer[i].value, old_locals->_buffer[i].value))
		{
			GList *list = NULL;
			CORBA_any *event_any;
			list = g_list_append (list, 
								  GINT_TO_POINTER (GDF_SYMBOL_SET_LOCALS));
			list = g_list_append (list, GINT_TO_POINTER (i));
			event_any = gdf_marshal_event_long_list ("symbol_changed", list);
			gdf_event_channel_client_push (cmdr->event_channel, event_any);
			CORBA_free (event_any);
		}
	}

  CORBA_free (old_locals);
}

void 
fill_locals (GdfGdbCommander *cmdr, GDF_SymbolSet *s)
{
  struct block *block = get_frame_block (selected_frame);
  int nsyms;
  int i;
  struct symbol *sym;

  if (block == 0)
	{
	  return;
	}
  
  while (block != 0)
	{

	  nsyms = BLOCK_NSYMS (block);
	  for (i = 0; i < nsyms; i++) 
		{
		  sym = BLOCK_SYM (block, i);
		  switch (SYMBOL_CLASS (sym)) 
			{
			case LOC_LOCAL :
			case LOC_REGISTER :
			case LOC_STATIC:
			case LOC_BASEREG:
			  if (s->_maximum == s->_length) 
				{
				  void *new_buf;
				  s->_maximum *= 2;
				  new_buf = 
					CORBA_octet_allocbuf (sizeof (GDF_Symbol) * s->_maximum);
				  memset (new_buf, 0, sizeof (GDF_Symbol) * s->_maximum);
				  memcpy (new_buf, s->_buffer, 
						  s->_length * sizeof (GDF_Symbol));
				  CORBA_free (s->_buffer);
				  s->_buffer = new_buf;
				}
			  s->_buffer[s->_length].handle = s->_length;
			  s->_buffer[s->_length].symbol_set = GDF_SYMBOL_SET_LOCALS;
			  fill_value_sym (cmdr, &s->_buffer[s->_length], sym, NULL);
			  s->_length++;
			  break;
			default :
			  break;
			}
		}

	  if (BLOCK_FUNCTION (block))
		break;
	  block = BLOCK_SUPERBLOCK (block);
	}
  
}

static void
fill_value_sym (GdfGdbCommander *cmdr, GDF_Symbol *gdf_sym, 
				struct symbol *sym, struct frame_info *frame)
{
  static struct ui_file *stream = NULL;
  if (!stream) 
	{
	  stream = ui_file_new ();
	  set_ui_file_write (stream, gdf_ui_file_write);
	}

  /* FIXME: Do this right */
  gdf_sym->parent_handle = 0;
  gdf_sym->name = 
	CORBA_string_dup (SYMBOL_SOURCE_NAME (sym));
  gdf_sym->expression = 
	CORBA_string_dup (SYMBOL_SOURCE_NAME (sym));
  
  gdf_sym->type = GDF_TYPE_CHAR;
  gdf_sym->expandable = CORBA_FALSE;
  gdf_sym->expanded = CORBA_TRUE;

  val_buf = g_malloc (INITIAL_BUF_SIZE);
  val_buf[0] = '\0';
  val_buf_len = INITIAL_BUF_SIZE;
  print_variable_value (sym, frame, stream);

  gdf_sym->value = CORBA_string_dup (val_buf);
  g_free (val_buf);
  val_buf = NULL;
  val_buf_len = 0;
}

static void
gdf_ui_file_write (struct ui_file *stream, const char *buf, long buf_len)
{
  int cur_len;

  g_assert (val_buf);
  cur_len = strlen (val_buf);

  if (cur_len + buf_len + 1 > val_buf_len) 
	{
	  char *new_buf;
	  val_buf_len *= 2;
	  if (val_buf_len < cur_len + buf_len + 1)
		val_buf_len = (cur_len + buf_len + 1) * 2;
	  new_buf = g_malloc (val_buf_len);
	  strcpy (new_buf, val_buf);
	  g_free (val_buf);
	  val_buf = new_buf;
	}
  
  memcpy (val_buf + cur_len, buf, buf_len);
  *(val_buf + cur_len + buf_len) = '\0';
}
