#ifndef __GDF_GDB_COMMANDER_H__
#define __GDF_GDB_COMMANDER_H__

#include "../gdb/gdb/defs.h"
#include "../gdb/gdb/top.h"
#include "../gdb/gdb/gdbcore.h"
#include "../gdb/gdb/target.h"
#include "../gdb/gdb/symfile.h"
#include "../gdb/gdb/objfiles.h"
#include "../gdb/gdb/breakpoint.h"
#include "../gdb/gdb/annotate.h"
#include "../gdb/gdb/inferior.h"

#include <bonobo/bonobo-object.h>
#include <gdf/gdf.h>

BEGIN_GNOME_DECLS

#define GDF_GDB_COMMANDER_TYPE (gdf_gdb_commander_get_type ())
#define GDF_GDB_COMMANDER(o) (GTK_CHECK_CAST ((o), GDF_GDB_COMMANDER_TYPE, GdfGdbCommander))
#define GDF_GDB_COMMANDER_CLASS(k) (GTK_CHECK_CLASS_CAST ((k), GDF_GDB_COMMANDER_CLASS_TYPE, GdfGdbCommanderClass))
#define GDF_IS_GDB_COMMANDER(o) (GTK_CHECK_TYPE ((o), GDF_GDB_COMMANDER_TYPE))
#define GDF_IS_GDB_COMMANDER_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), GDF_GDB_COMMANDER_TYPE))

typedef struct 
{
  BonoboObject base;
  
  GdfEventChannelClient *event_channel;

  GDF_Commander_State state;

  struct breakpoint *new_breakpoint;

  GDF_SymbolSet *locals;
} GdfGdbCommander;

typedef struct 
{
    BonoboObjectClass parent_class;
} GdfGdbCommanderClass;

GdfGdbCommander *gdf_gdb_commander_new (void);
GtkType gdf_gdb_commander_get_type (void);

END_GNOME_DECLS

#endif

