/*
 * Implements a GDF Commander.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#include <config.h>
#include <gnome.h>
#include <bonobo.h>
#include <string.h>
#include <setjmp.h>

#include "../gdb/gdb/defs.h"
#include "../gdb/gdb/top.h"
#include "../gdb/gdb/gdbcore.h"
#include "../gdb/gdb/target.h"
#include "../gdb/gdb/symfile.h"
#include "../gdb/gdb/objfiles.h"
#include "../gdb/gdb/breakpoint.h"
#include "../gdb/gdb/annotate.h"
#include "../gdb/gdb/inferior.h"

#include "gdf-gdb-commander.h"
#include "gdf-out.h"
#include "gdf-val.h"
#include <gdf/gnome-debug.h>


#define CHECK_LOADED_VOID() \
G_STMT_START { \
  char *filename = get_exec_file (0); \
  if (!filename) \
    { \
	  CORBA_exception_set (ev, CORBA_USER_EXCEPTION, \
						   ex_GDF_Commander_NoBinary, NULL); \
      return; \
	} \
} G_STMT_END;

#define CHECK_LOADED_VAL(retval) \
G_STMT_START { \
  char *filename = get_exec_file (0); \
  if (!filename) \
    { \
	  CORBA_exception_set (ev, CORBA_USER_EXCEPTION, \
						   ex_GDF_Commander_NoBinary, NULL); \
      return retval; \
    } } G_STMT_END;

extern int pagination_enabled;

static int pending_exit;

static CORBA_Object create_gdb_commander (BonoboObject *object);
static void gdf_gdb_commander_destroy (GtkObject *object);
static void gdf_gdb_commander_class_init (GdfGdbCommanderClass *class);
static void gdf_gdb_commander_init (BonoboObject *object);
static NORETURN void gdf_error_hook (void) ATTR_NORETURN;
static void gdf_starting_hook (void);
static void gdf_stopped_hook (void);
static void gdf_create_breakpoint_hook (struct breakpoint *bp);
static void gdf_exited_hook (void);

static BonoboObjectClass *parent_class;
static POA_GDF_Commander__epv gdb_commander_epv;
static POA_GDF_Commander__vepv gdb_commander_vepv;

#define CORBA_boolean__alloc() (CORBA_boolean*) CORBA_octet_allocbuf (sizeof (CORBA_boolean))

static GdfGdbCommander *gdf_cmdr = NULL;

static jmp_buf jmp_dest;

static inline GdfGdbCommander*
gdb_commander_from_servant (PortableServer_Servant servant)
{
  return GDF_GDB_COMMANDER (bonobo_object_from_servant (servant));
}

/* public routines */

GdfGdbCommander *
gdf_gdb_commander_new (void)
{  
  GDF_Commander objref;

  pending_exit = -1;

  pagination_enabled = 0;

  /* There should never be more than one commander */
  g_assert (gdf_cmdr == NULL);
  
  gdf_cmdr = 
    gtk_type_new (gdf_gdb_commander_get_type ());
  objref = 
    create_gdb_commander (BONOBO_OBJECT (gdf_cmdr));
  if (objref == CORBA_OBJECT_NIL) 
    {
      gtk_object_destroy (GTK_OBJECT (gdf_cmdr));
      return NULL;
    }
  
  bonobo_object_construct (BONOBO_OBJECT (gdf_cmdr),
						  objref);
  
  gdf_cmdr->state = GDF_Commander_NONE;
  gdf_cmdr->event_channel = gdf_event_channel_client_new ();

  uiout = gdf_out_new (gdf_cmdr);

  /* We want to handle errors ourselves, so we set GDB's error hook */
  error_hook = gdf_error_hook;
  create_breakpoint_hook = gdf_create_breakpoint_hook;
  annotate_starting_hook = gdf_starting_hook;
  annotate_stopped_hook = gdf_stopped_hook;
  annotate_exited_hook = gdf_exited_hook;

  return gdf_cmdr;
}

GtkType 
gdf_gdb_commander_get_type (void)
{
  static GtkType type = 0;
  
  if (!type) 
    {
      GtkTypeInfo info = 
      {
		"IDL:GDF/Commander:1.0",
		sizeof (GdfGdbCommander),
		sizeof (GdfGdbCommanderClass),
		(GtkClassInitFunc) gdf_gdb_commander_class_init,
		(GtkObjectInitFunc) gdf_gdb_commander_init,
		NULL,
		NULL,
		(GtkClassInitFunc) NULL
      };
      
      type = gtk_type_unique (bonobo_object_get_type (), &info);
    }
  
  return type;
}

/* private routines */
CORBA_Object
create_gdb_commander (BonoboObject *object) 
{
  POA_GDF_Commander *servant;
  CORBA_Environment ev;
  CORBA_exception_init (&ev);
  
  servant = 
    (POA_GDF_Commander*)g_new0(BonoboObjectServant, 1);
  servant->vepv = &gdb_commander_vepv;
  
  POA_GDF_Commander__init((PortableServer_Servant) servant, 
			  &ev);
  if (ev._major != CORBA_NO_EXCEPTION) 
    {
      g_free (servant);
      CORBA_exception_free (&ev);
      return CORBA_OBJECT_NIL;
    }
  CORBA_exception_free (&ev);
  return bonobo_object_activate_servant (object, servant);
}

static void
gdf_gdb_commander_destroy (GtkObject *object) 
{
  GdfGdbCommander *cmdr = GDF_GDB_COMMANDER (object);

  GDF_TRACE ();

  if (cmdr->event_channel)
    gdf_event_channel_client_destroy_channel (cmdr->event_channel);

  /* exit the event loop */
  gtk_main_quit ();
}

static GDF_Commander_State
impl_get_state (PortableServer_Servant servant, CORBA_Environment *ev)
{
  GdfGdbCommander *cmdr = gdb_commander_from_servant (servant);

  return cmdr->state;
}

static void
unload_binary (GdfGdbCommander *cmdr, CORBA_Environment *ev)
{
  CORBA_any *event_any;

  execute_command ("file", 0);

  cmdr->state = GDF_Commander_NONE;

  event_any = gdf_marshal_event_none ("program_unloaded");
  gdf_event_channel_client_push (cmdr->event_channel, event_any);
  CORBA_free (event_any);

}

static void
impl_load_binary (PortableServer_Servant servant, 
				  const CORBA_char *file_name,
				  CORBA_Environment *ev)
{
  GdfGdbCommander *cmdr = gdb_commander_from_servant (servant);
  CORBA_any *event_any;

  GDF_TRACE ();
  
  if (get_exec_file(0) != NULL)
	unload_binary (cmdr, ev);
  
  if (setjmp (jmp_dest) != 0) {
	g_error ("target_preopen failed");
  }
  
  target_preopen (0);
  
  if (setjmp (jmp_dest) != 0) 
	{
	  CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
						   ex_GDF_Commander_InvalidBinary, NULL);
	  return;
	}
  
  exec_file_attach ((char*)file_name, 0);

  if (setjmp (jmp_dest) != 0)
	{
	  CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
						   ex_GDF_Commander_NoSymbols, NULL);
	  unload_binary (cmdr, ev);
	} else {
	  symbol_file_command ((char*)file_name, 0);
	}

  cmdr->state = GDF_Commander_NONE;

  event_any = gdf_marshal_event_none ("program_loaded");
  gdf_event_channel_client_push (cmdr->event_channel, event_any);
  CORBA_free (event_any);
}

static void
impl_unload_binary (PortableServer_Servant servant, CORBA_Environment *ev)
{
  GdfGdbCommander *cmdr = gdb_commander_from_servant (servant);

  GDF_TRACE ();

  CHECK_LOADED_VOID ();

  unload_binary (cmdr, ev);
}

static CORBA_boolean
impl__get_binary_loaded (PortableServer_Servant servant, CORBA_Environment *ev)
{
  char *filename = get_exec_file (0);
  
  return (filename != NULL) ? CORBA_TRUE : CORBA_FALSE;
}

static void
impl_attach (PortableServer_Servant servant, 
			 CORBA_short pid,
			 CORBA_Environment *ev)
{
  /* not implemented */
}

static gint
execute_idle_handler (GdfGdbCommander *cmdr)
{
  if (setjmp (jmp_dest) != 0) 
	{
	  return FALSE;
	}

  /* FIXME: Support arguments */
  
  execute_command ("run", 0);

  return FALSE;
}

static void
impl_execute (PortableServer_Servant servant, 
			  const GDF_arg_list *args,
			  CORBA_Environment *ev)
{
  GdfGdbCommander *cmdr = gdb_commander_from_servant (servant);
 
  CHECK_LOADED_VOID ();
 
  /* FIXME: Support arguments */

  gtk_idle_add ((GtkFunction)execute_idle_handler, cmdr);
}

static void
impl_load_corefile (PortableServer_Servant servant, 
					const CORBA_char *corefile_name,
					CORBA_Environment *ev)
{
  /* not implemented */
}

static void
add_source (GDF_source_file_list *list, char *filename)
{
  if (list->_maximum == list->_length) 
	{
	  void *new_buf;
	  list->_maximum *= 2;
	  new_buf = 
		CORBA_octet_allocbuf (sizeof (CORBA_char *) * list->_maximum);
	  memcpy (new_buf, list->_buffer, list->_length * sizeof (CORBA_char *));
	  CORBA_free (list->_buffer);
	  list->_buffer = new_buf;
	}
  list->_buffer[list->_length++] = CORBA_string_dup (filename);
}

static GDF_source_file_list *
impl_get_sources (PortableServer_Servant servant, CORBA_Environment *ev)
{
  register struct symtab *s;
  register struct partial_symtab *ps;
  register struct objfile *objfile;

  GDF_source_file_list *list;

  list = GDF_source_file_list__alloc ();

  list->_maximum = 20;
  list->_length = 0;
  list->_buffer = 
	(CORBA_char**)CORBA_octet_allocbuf (sizeof (CORBA_char *) * 20);
  CORBA_sequence_set_release (list, CORBA_TRUE);

  CHECK_LOADED_VAL (list);
  
  if (!have_full_symbols () && !have_partial_symbols ())
	{
	  CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
						   ex_GDF_Commander_NoSymbols, NULL);
	  
	  return list;
	}
  
  ALL_SYMTABS (objfile, s) 
	{
	  add_source (list, s->filename);
	}
  
  ALL_PSYMTABS (objfile, ps) 
	{
	  if (!ps->readin) 
		{
		  /* FIXME: Deal with the multiple-partial-symtab-entries problem
			 described in symtab.c */
		  add_source (list, ps->filename);
		}
	}

  return list;
}

static CORBA_char *
impl_get_absolute_source_path (PortableServer_Servant servant, 
							   const CORBA_char *source_file,
							   CORBA_Environment *ev)
{
  CORBA_char *retval;
  struct symtab *s = NULL;
  struct partial_symtab *ps = NULL;  
  struct objfile *objfile = NULL;

  GDF_TRACE ();

  retval = CORBA_string_dup ("");
  CHECK_LOADED_VAL (retval);
  CORBA_free (retval);

  if (source_file[0] == '/') 
	{
	  retval = CORBA_string_dup (source_file);
	  GDF_TRACE_EXTRA ("retval: %s", source_file);
	  return retval;
	}
  else 
	{
	  gchar *abs_path;
	  
	  if (!have_full_symbols () && !have_partial_symbols ())
		{
		  GDF_TRACE_EXTRA ("returning empty %d", 0);
		  return CORBA_string_dup ("");
		}
	  ALL_SYMTABS (objfile, s)
		{
		  if (!strcmp (s->filename, source_file))
			{
			  /* FIXME: Deal with NULL dirname */
			  abs_path = g_strdup_printf ("%s%s", s->dirname, s->filename);
			  retval = CORBA_string_dup (abs_path);
			  GDF_TRACE_EXTRA ("abs_path: %s", abs_path);
			  g_free (abs_path);
			  return retval;
			}
		}
	  ALL_PSYMTABS (objfile, ps)
		{
		  if (!ps)
			continue;

		  if (!ps->readin)
			{
			  if (!strcmp (ps->filename, source_file))
				{
				  s = psymtab_to_symtab (ps);
				  abs_path = g_strdup_printf ("%s%s", s->dirname, s->filename);
				  retval = CORBA_string_dup (abs_path);
				  GDF_TRACE_EXTRA ("abs_path: %s", abs_path);
				  g_free (abs_path);
				  return retval;
				}
			  
			}
		}
	}
  retval = CORBA_string_dup ("");
  GDF_TRACE_EXTRA ("returning empty %s", retval);

  return retval;
}

static void 
gdf_create_breakpoint_hook (struct breakpoint *b)
{
  CORBA_any *event_any;

  GDF_TRACE ();
  
  /* This is a bit of a kludge to give breakpoint information to 
   * the _set_breakpoint_* functions */
  gdf_cmdr->new_breakpoint = b;

  event_any = gdf_marshal_event_long ("breakpoint_set", 
									  (glong)gdf_cmdr->new_breakpoint->number);
  gdf_event_channel_client_push (gdf_cmdr->event_channel, event_any);
  CORBA_free (event_any);
}

static CORBA_long
impl_set_breakpoint (PortableServer_Servant servant, 
					 const CORBA_char *file_name,
					 const CORBA_long line_num,
					 const CORBA_char *condition,
					 CORBA_Environment *ev)
{
  GdfGdbCommander *cmdr = gdb_commander_from_servant (servant);
  char *arg;
  char *file;

  CHECK_LOADED_VAL (-1);

  if (setjmp (jmp_dest) != 0) 
	{
	  CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
						   ex_GDF_Commander_UnknownError, NULL);
	  return -1;
	}
    
  file = strrchr (file_name, G_DIR_SEPARATOR);
  if (!file++)
    file = (char*)file_name;

  if (strcmp (condition, ""))
	arg = g_strdup_printf ("%s:%d if %s", 
						   file, line_num, condition);
  else
	arg = g_strdup_printf ("%s:%d", file, line_num);

  /* Event will be emitted in gdf_create_breakpoint_hook */
  break_command (arg, 0);
  
  g_free (arg);

  return cmdr->new_breakpoint->number;
}

static CORBA_long
impl_set_breakpoint_function (PortableServer_Servant servant,
							  const CORBA_char *file_name,
							  const CORBA_char *function_name,
							  const CORBA_char *condition,
							  CORBA_Environment *ev)
{
  GdfGdbCommander *cmdr = gdb_commander_from_servant (servant);
  char *arg;
  char *file;

  GDF_TRACE ();

  CHECK_LOADED_VAL (-1);
  
  if (setjmp (jmp_dest) != 0) 
	{
	  CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
						   ex_GDF_Commander_UnknownError, NULL);
	  GDF_TRACE ();
	  return -1;
	}
    
  file = strrchr (file_name, G_DIR_SEPARATOR);
  if (!file++)
    file = (char*)file_name;

  if (strcmp (file, "")) 
	{
	  if (strcmp (condition, ""))
		arg = g_strdup_printf ("%s:%s if %s", 
							   file, function_name, condition);
	  else
		arg = g_strdup_printf ("%s:%s", file_name, function_name);
	} 
  else
	{  
	  if (strcmp (condition, ""))
		arg = g_strdup_printf ("%s if %s", 
							   function_name, condition);
	  else
		arg = g_strdup_printf ("%s", function_name);
	}
  
  /* Event will be emitted in gdf_create_breakpoint_hook */
  break_command (arg, 0);
  
  GDF_TRACE ();
  g_free (arg);

  return cmdr->new_breakpoint->number;
}

extern struct breakpoint *breakpoint_chain;

static struct breakpoint *get_breakpoint_by_num (gint bp_num)
{
  struct breakpoint *b;
  for (b = breakpoint_chain; b; b = b->next) 
	if (b->number == bp_num) 
	  return b;
	
  return NULL;
}

static void
impl_enable_breakpoint (PortableServer_Servant servant, 
						CORBA_long bp_num,
						CORBA_Environment *ev)
{
  GdfGdbCommander *cmdr = gdb_commander_from_servant (servant);
  CORBA_any *event_any;
  struct breakpoint *b;

  CHECK_LOADED_VOID ();
  
  b = get_breakpoint_by_num (bp_num);
  if (!b) 
	{	
	  CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
						   ex_GDF_Commander_DoesntExist, NULL);
	  return;
	}

  enable_breakpoint (b);

  event_any = gdf_marshal_event_long ("breakpoint_enabled", (glong)bp_num);
  gdf_event_channel_client_push (cmdr->event_channel, event_any);
  CORBA_free (event_any);
}

static void
impl_disable_breakpoint (PortableServer_Servant servant,
						 CORBA_long bp_num,
						 CORBA_Environment *ev)
{
  GdfGdbCommander *cmdr = gdb_commander_from_servant (servant);
  CORBA_any *event_any;
  struct breakpoint *b;

  CHECK_LOADED_VOID();
  
  b = get_breakpoint_by_num (bp_num);
  if (!b) 
	{
	  CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
						   ex_GDF_Commander_DoesntExist, NULL);
	  return;
	}

  disable_breakpoint (b);

  event_any = gdf_marshal_event_long ("breakpoint_disabled", (glong)bp_num);
  gdf_event_channel_client_push (cmdr->event_channel, event_any);
  CORBA_free (event_any);
}

static void
impl_delete_breakpoint (PortableServer_Servant servant, 
						CORBA_long bp_num,
						CORBA_Environment *ev)
{
  GdfGdbCommander *cmdr = gdb_commander_from_servant (servant);
  CORBA_any *event_any;
  struct breakpoint *b;  

  CHECK_LOADED_VOID();
  
  b = get_breakpoint_by_num (bp_num);
  if (!b) 
	{
	  CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
						   ex_GDF_Commander_DoesntExist, NULL);
	  return;
	}

  delete_breakpoint (b);

  event_any = gdf_marshal_event_long ("breakpoint_deleted", (glong)bp_num);
  gdf_event_channel_client_push (cmdr->event_channel, event_any);
  CORBA_free (event_any);
}

static GDF_Breakpoint *
impl_get_breakpoint_info (PortableServer_Servant servant,
						  CORBA_long bp_num,
						  CORBA_Environment *ev)
{
  GDF_Breakpoint *ret;
  struct breakpoint *b;

  ret = GDF_Breakpoint__alloc ();

  CHECK_LOADED_VAL (ret);

  b = get_breakpoint_by_num (bp_num);
  if (!b) 
	{
	  CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
						   ex_GDF_Commander_DoesntExist, NULL);
	  return ret;
	}

  ret->num = bp_num;
  ret->enabled = (CORBA_boolean)(b->enable != 0);
  ret->file_name = CORBA_string_dup (b->source_file);
  ret->line_num = (CORBA_long)b->line_number;
  ret->address = (CORBA_long)b->address;

  /* FIXME: Deal with this */
  ret->type = CORBA_string_dup ("");

  return ret;
}

static GDF_RegisterList *
impl_get_registers (PortableServer_Servant servant,
					CORBA_Environment *ev)
{
  GDF_RegisterList *ret;
  int i;
  int numregs = ARCH_NUM_REGS;

  /* FIXME: Support FP registers */

  ret = GDF_RegisterList__alloc ();
  ret->_length = 0;
  ret->_maximum = 10;
  ret->_buffer = (GDF_Register*)CORBA_octet_allocbuf (sizeof (GDF_Register)
													  * 10);
  CORBA_sequence_set_release (ret, CORBA_TRUE);
  
  CHECK_LOADED_VAL (ret);
  
  for (i = 0; i < numregs; i++)
	{
	  char raw_buffer [MAX_REGISTER_RAW_SIZE];
	  char virtual_buffer[MAX_REGISTER_VIRTUAL_SIZE];
	  char *value;
	  
	  /* If the register name is empty, it is undefined for this
         processor, so don't display anything.  */
   
	  if (REGISTER_NAME (i) == NULL || *(REGISTER_NAME (i)) == '\0')
		continue;
	  
	  if (ret->_maximum == ret->_length) 
		{
		  void *new_buf;
		  ret->_maximum *= 2;
		  new_buf = 
			CORBA_octet_allocbuf (sizeof (GDF_Register) * ret->_maximum);
		  memcpy (new_buf, ret->_buffer, ret->_length * sizeof (GDF_Register));
		  CORBA_free (ret->_buffer);
		  ret->_buffer = new_buf;
		}
	  ret->_buffer[ret->_length].name = CORBA_string_dup (REGISTER_NAME(i));
	  if (read_relative_register_raw_bytes (i, raw_buffer))
		{
		  ret->_buffer[ret->_length++].value = 
			CORBA_string_dup ("*value not available*");
		  continue;
		}

	  if (REGISTER_CONVERTIBLE (i))
		{
		  REGISTER_CONVERT_TO_VIRTUAL (i, REGISTER_VIRTUAL_TYPE (i),
									   raw_buffer, virtual_buffer);
		}
	  else 
		{
		  memcpy (virtual_buffer, raw_buffer, REGISTER_VIRTUAL_SIZE (i));
		}
	  

	  /* FIXME: Format floating pt. */

	  if (REGISTER_VIRTUAL_SIZE (i) > (int) sizeof (long)) 
		{
		  /* FIXME: Format large registers */
		  ret->_buffer[ret->_length++].value = CORBA_string_dup ("FIXME: Format large registers");
		}
	  else 
		{
		  LONGEST val_long;
		  extract_long_unsigned_integer (virtual_buffer, 
										 TYPE_LENGTH (REGISTER_VIRTUAL_TYPE(i)), 
										 &val_long);
		  /* FIXME: Format this correctly */
		  value = g_strdup_printf ("0x%lx", val_long);

		  ret->_buffer[ret->_length++].value = CORBA_string_dup (value);
		  g_free (value);
		}
	}
  return ret;
}

static GDF_StackFrame *
impl_get_frame (PortableServer_Servant servant, 
		CORBA_long id,
		CORBA_Environment *ev)
{
  /* not implemented */
  return NULL;
}

static GDF_Stack *
impl_get_backtrace (PortableServer_Servant servant, CORBA_Environment *ev)
{
  /* not implemented */
  return NULL;
}

static gint
cont_idle_handler (GdfGdbCommander *cmdr)
{
  if (setjmp (jmp_dest) != 0) 
	{
	  return FALSE;
	}
  

  clear_proceed_status ();
  proceed (-1, TARGET_SIGNAL_DEFAULT, 0);

  return FALSE;
}

static void
impl_cont (PortableServer_Servant servant, CORBA_Environment *ev)
{
  GdfGdbCommander *cmdr = gdb_commander_from_servant (servant);
  
  CHECK_LOADED_VOID ();
 
  if (inferior_pid == 0) 
	{
	  CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
						   ex_GDF_Commander_NotRunning, NULL);
	  return;
	}

  gtk_idle_add ((GtkFunction)cont_idle_handler, cmdr);
}

static void
impl_stop (PortableServer_Servant servant, CORBA_Environment *ev)
{
  GdfGdbCommander *cmdr = gdb_commander_from_servant (servant);

  CHECK_LOADED_VOID ();

  if (inferior_pid == 0) 
	{
	  CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
						   ex_GDF_Commander_NotRunning, NULL);
	}

  if (setjmp (jmp_dest) != 0) 
	{
	  g_error ("Error killing target");
	  return;
	}

  cmdr->state = GDF_Commander_STOPPED;
  
  target_kill ();  

  /* FIXME: Killing off the inferior can leave us with a core file.  Deal
   * with this possibility. */
}

static void
impl_restart (PortableServer_Servant servant, CORBA_Environment *ev)
{
  /* FIXME: Do this SOON */
}

static void
impl_step_over (PortableServer_Servant servant, CORBA_Environment *ev)
{
  CHECK_LOADED_VOID ();

  if (inferior_pid == 0) 
	{
	  CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
						   ex_GDF_Commander_NotRunning, NULL);
	  return;
	}

  if (setjmp (jmp_dest) != 0) 
	{
	  g_error ("next failed!");
	}
  
  execute_command ("next", 0);
}

static void
impl_step_into (PortableServer_Servant servant, CORBA_Environment *ev)
{
  CHECK_LOADED_VOID ();
  if (inferior_pid == 0) 
	{
	  CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
						   ex_GDF_Commander_NotRunning, NULL);
	  return ;
	}
  
  if (setjmp (jmp_dest) != 0) 
	{
	  g_error ("step failed!");
	  return ;
	}

  execute_command ("step", 0);
}

static void
impl_step_out (PortableServer_Servant servant, CORBA_Environment *ev)
{
  CHECK_LOADED_VOID ();

  if (inferior_pid == 0) 
	{
	  CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
						   ex_GDF_Commander_NotRunning, NULL);
	  return ;
	}
  
  if (setjmp (jmp_dest) != 0) 
	{
	  g_error ("step failed!");
	  return ;
	}

  execute_command ("finish", 0);
}

static GDF_SymbolSet *
impl_get_symbol_set (PortableServer_Servant servant,
					 GDF_SymbolSetId id,
					 CORBA_Environment *ev)
{
  GdfGdbCommander *cmdr = gdb_commander_from_servant (servant);
  GDF_SymbolSet *set = NULL;
  GDF_SymbolSet *ret;

  switch (id) 
	{
	case GDF_SYMBOL_SET_LOCALS :
	  /* FIXME: Necessary? */
	  gdf_update_locals (cmdr);
	  set = cmdr->locals;
	  break;
	default :
	  g_assert_not_reached ();
	}

  ret = GDF_SymbolSet__alloc ();
  ret->_length = set->_length;
  ret->_maximum = set->_length;
  ret->_buffer = set->_buffer;
  CORBA_sequence_set_release (ret, CORBA_FALSE);
 
  return ret;
}

static GDF_Symbol *
impl_get_symbol (PortableServer_Servant servant,
				 GDF_SymbolSetId set,
				 CORBA_long id,
				 CORBA_Environment *ev)
{
  GdfGdbCommander *cmdr = gdb_commander_from_servant (servant);
  GDF_Symbol *sym = NULL;
  GDF_Symbol *ret;

  ret = GDF_Symbol__alloc ();

  switch (set) 
	{
	case GDF_SYMBOL_SET_LOCALS :
      g_assert (id < cmdr->locals->_length);
	  sym = &cmdr->locals->_buffer[id];
	  break;
	default :
	  g_assert_not_reached ();
	}
  
  g_assert (sym->handle == id);
  *ret = *sym;
  ret->name = CORBA_string_dup (sym->name);
  ret->expression = CORBA_string_dup (sym->expression);
  ret->value = CORBA_string_dup (sym->value);

  /* FIXME: children */
  return ret;
}	

static CORBA_Object
impl_get_event_channel (PortableServer_Servant servant,
						CORBA_Environment *ev)
{
  CORBA_Object ret;
  GdfGdbCommander *cmdr = gdb_commander_from_servant (servant);
  ret = bonobo_object_corba_objref (BONOBO_OBJECT (cmdr->event_channel));
  return CORBA_Object_duplicate (ret, ev);
}

static void
impl_set_output_tty (PortableServer_Servant servant,
					 const CORBA_char *tty_name,
					 CORBA_Environment *ev)
{
  char *cmd_str = NULL;
  if (setjmp (jmp_dest) != 0) 
	{
	  g_error ("tty command failed");
	  return;
	}
  
  cmd_str = g_strdup_printf ("tty %s", tty_name);
  
  execute_command (cmd_str, 0);
  g_free (cmd_str);
}

static void 
gdf_error_hook (void)
{
  longjmp (jmp_dest, 1);
}

static void
gdf_starting_hook (void)
{
  CORBA_any *event_any;

  if (gdf_cmdr->state != GDF_Commander_RUNNING) 
	{
	  event_any = gdf_marshal_event_none ("started");
	  gdf_event_channel_client_push (gdf_cmdr->event_channel, event_any);  
	  CORBA_free (event_any);
	}
  gdf_cmdr->state = GDF_Commander_RUNNING;
  event_any = gdf_marshal_event_none ("running");
  gdf_event_channel_client_push (gdf_cmdr->event_channel, event_any);
  CORBA_free (event_any);
}

static void 
gdf_stopped_hook (void)
{
  CORBA_any *event_any;
  struct symtab_and_line sal;
  char *path;
  struct frame_info *frame;
  static CORE_ADDR previous_frame_addr = -1;

  gdf_cmdr->state = GDF_Commander_STOPPED;

  /* FIXME: This is pretty ugly */
  frame = get_current_frame ();
  GDF_TRACE_EXTRA ("%d", FRAME_FP (frame));
  if (FRAME_FP (frame) != previous_frame_addr) 
	{
	  gdf_new_locals (gdf_cmdr);
	  previous_frame_addr = FRAME_FP (frame);
	}

  /* Update the variable lists */
  gdf_update_locals (gdf_cmdr);
  
  event_any = gdf_marshal_event_none ("stopped");
  gdf_event_channel_client_push (gdf_cmdr->event_channel, event_any);
  CORBA_free (event_any);

  sal = find_pc_line (stop_pc, 0);

  if (sal.symtab) 
	{	  
	  path = g_strdup_printf ("%s%c%s", sal.symtab->dirname, 
							  G_DIR_SEPARATOR,
							  sal.symtab->filename);
	  event_any = gdf_marshal_event_source_location ("source_line",
													 path,
													 sal.line);
	  g_free (path);
	  
	  gdf_event_channel_client_push (gdf_cmdr->event_channel, event_any);
	  CORBA_free (event_any);
	}

  if (pending_exit != -1)
	{
	  gdf_cmdr->state = GDF_Commander_NONE;
	  event_any = gdf_marshal_event_long ("exited", (glong)pending_exit);
	  gdf_event_channel_client_push (gdf_cmdr->event_channel, event_any);
	  CORBA_free (event_any);
	  pending_exit = -1;
	}
}

static void
gdf_exited_hook (void)
{
  /*pending_exit = exitstatus;*/
  pending_exit = 0;
}

static void
init_gdb_commander_corba_class (void) 
{
  /* EPV */
  gdb_commander_epv.get_state = impl_get_state;
  gdb_commander_epv.load_binary = impl_load_binary;
  gdb_commander_epv.unload_binary = impl_unload_binary;
  gdb_commander_epv._get_binary_loaded = impl__get_binary_loaded;
  gdb_commander_epv.attach = impl_attach;
  gdb_commander_epv.execute = impl_execute;
  gdb_commander_epv.load_corefile = impl_load_corefile;
  gdb_commander_epv.get_sources = impl_get_sources;
  gdb_commander_epv.get_absolute_source_path = impl_get_absolute_source_path;
  gdb_commander_epv.set_breakpoint = impl_set_breakpoint;
  gdb_commander_epv.set_breakpoint_function = impl_set_breakpoint_function;
  gdb_commander_epv.enable_breakpoint = impl_enable_breakpoint;
  gdb_commander_epv.disable_breakpoint = impl_disable_breakpoint;
  gdb_commander_epv.delete_breakpoint = impl_delete_breakpoint;
  gdb_commander_epv.get_breakpoint_info = impl_get_breakpoint_info;
  gdb_commander_epv.get_registers = impl_get_registers;
  gdb_commander_epv.get_frame = impl_get_frame;
  gdb_commander_epv.get_backtrace = impl_get_backtrace;
  gdb_commander_epv.cont = impl_cont;
  gdb_commander_epv.stop = impl_stop;
  gdb_commander_epv.restart = impl_restart;
  gdb_commander_epv.step_over = impl_step_over;
  gdb_commander_epv.step_into = impl_step_into;
  gdb_commander_epv.step_out = impl_step_out;
  gdb_commander_epv.get_symbol_set = impl_get_symbol_set;
  gdb_commander_epv.get_symbol = impl_get_symbol;
  gdb_commander_epv.get_event_channel = impl_get_event_channel;
  gdb_commander_epv.set_output_tty = impl_set_output_tty;
   
  /* VEPV */
  gdb_commander_vepv.Bonobo_Unknown_epv = bonobo_object_get_epv ();
  gdb_commander_vepv.GDF_Commander_epv = &gdb_commander_epv;
}

       
static void
gdf_gdb_commander_class_init (GdfGdbCommanderClass *class) 
{
  GtkObjectClass *object_class = (GtkObjectClass*) class;
  parent_class = gtk_type_class (bonobo_object_get_type ());
  
  object_class->destroy = gdf_gdb_commander_destroy;
  
  init_gdb_commander_corba_class ();
}

static void
gdf_gdb_commander_init (BonoboObject *object)
{
  GDF_GDB_COMMANDER (object)->locals = NULL;
}
 



