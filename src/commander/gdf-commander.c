#include <gnome.h>
#include <gdf/gdf.h>
#include <libgnorba/gnorba.h>
#include "gdf-commander.h"
#include "gdf-gdb-commander.h"
#include "../gdb/gdb/defs.h"
#include <setjmp.h>
#include "../gdb/gdb/top.h"

extern char *version;

void
gdf_command_loop (void)
{
  GDF_TRACE ();

  bonobo_main ();
}
  
void
gdf_commander_initialize (void)
{
  CORBA_Environment ev;
  CORBA_Object name_service;
  GdfGdbCommander *cmdr;
  CORBA_ORB orb;
  int argc = 3;
  char *argv[] = { "gdf-gdb", "--activate-goad-server", "gdf_gdb_commander" };

  GDF_TRACE ();

  orb = gnome_CORBA_init ("gdf-gdb", version, &argc, argv, 
						  GNORBA_INIT_SERVER_FUNC, &ev);

  if (!bonobo_init (orb, NULL, NULL))
	  g_error (_("Can't initialize bonobo!"));

  cmdr = gdf_gdb_commander_new ();

  CORBA_exception_init (&ev);
  
  name_service = gnome_name_service_get ();
  goad_server_register (name_service, 
						bonobo_object_corba_objref (BONOBO_OBJECT (cmdr)),
						"gdf_gdb_commander",
						"object",
						&ev);
  CORBA_exception_free (&ev);

  command_loop_hook = gdf_command_loop;
}

