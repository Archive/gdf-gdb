#include "gdf-out.h"
#include "gdf-gdb-commander.h"

struct ui_out_data 
  {
	GdfGdbCommander *cmdr;
  };

static void gdf_table_begin (struct ui_out *uiout, int nbrofcols, char *tblid);
static void gdf_table_body (struct ui_out *uiout);
static void gdf_table_end (struct ui_out *uiout);
static void gdf_table_header (struct ui_out *uiout, int width,
                              enum ui_align alig, char *colhdr);
static void gdf_list_begin (struct ui_out *uiout, int list_flag, char *lstid);
static void gdf_list_end (struct ui_out *uiout, int list_flag);
static void gdf_field_int (struct ui_out *uiout, int fldno, int width,
                           enum ui_align alig, char *fldname, int value);
static void gdf_field_skip (struct ui_out *uiout, int fldno, int width,
                            enum ui_align alig, char *fldname);
static void gdf_field_string (struct ui_out *uiout, int fldno, int width,
                           enum ui_align alig, char *fldname,
                              const char *string);
static void gdf_field_fmt (struct ui_out *uiout, int fldno,
                           int width, enum ui_align align,
                           char *fldname, char *format, va_list args);
static void gdf_spaces (struct ui_out *uiout, int numspaces);
static void gdf_text (struct ui_out *uiout, char *string);
static void gdf_message (struct ui_out *uiout, int verbosity, char *format,
                         va_list args);
static void gdf_wrap_hint (struct ui_out *uiout, char *identstring);
static void gdf_flush (struct ui_out *uiout);

static struct ui_out_impl gdf_ui_out_impl =
{
  gdf_table_begin,
  gdf_table_body,
  gdf_table_end,
  gdf_table_header,
  gdf_list_begin,
  gdf_list_end,
  gdf_field_int,
  gdf_field_skip,
  gdf_field_string,
  gdf_field_fmt,
  gdf_spaces,
  gdf_text,
  gdf_message,
  gdf_wrap_hint,
  gdf_flush
};

static void gdf_table_begin (struct ui_out *uiout, int nbrofcols, char *tblid)
{
  GDF_TRACE_EXTRA ("number of columns: %d, table id: %s", nbrofcols, tblid);
}

static void gdf_table_body (struct ui_out *uiout)
{
  GDF_TRACE ();
}

static void gdf_table_end (struct ui_out *uiout)
{
  GDF_TRACE ();
}
  
static void gdf_table_header (struct ui_out *uiout, int width,
                              enum ui_align alig, char *colhdr)
{
  GDF_TRACE ();
}

static void gdf_list_begin (struct ui_out *uiout, int list_flag, char *lstid)
{
  GDF_TRACE_EXTRA ("list flag: %d, list id: %s", list_flag, lstid);  
}

static void gdf_list_end (struct ui_out *uiout, int list_flag)
{
  GDF_TRACE_EXTRA ("list_flag: %d", list_flag);
}

static void gdf_field_int (struct ui_out *uiout, int fldno, int width,
                           enum ui_align align, char *fldname, int value)
{
  GDF_TRACE_EXTRA ("fldno: %d, width: %d, ui_align %d, fldname %s, value %d",
			 fldno, width, align, fldname, value);
}

static void gdf_field_skip (struct ui_out *uiout, int fldno, int width,
                            enum ui_align align, char *fldname)
{
  GDF_TRACE_EXTRA ("fldno: %d, width: %d, ui_align %d, fldname %s",
			 fldno, width, align, fldname);
}

static void gdf_field_string (struct ui_out *uiout, int fldno, int width,
							  enum ui_align align, char *fldname,
                              const char *string)
{
  GDF_TRACE_EXTRA ("fldno: %d, width: %d, ui_align %d, fldname %s, value %s",
			 fldno, width, align, fldname, string);
}

static void gdf_field_fmt (struct ui_out *uiout, int fldno,
                           int width, enum ui_align align,
                           char *fldname, char *format, va_list args)
{
  GDF_TRACE_EXTRA ("fldno: %d, width: %d, ui_align %d, fldname %s",
				   fldno, width, align, fldname);
  vfprintf (stderr, format, args);
}

static void gdf_spaces (struct ui_out *uiout, int numspaces)
{
  GDF_TRACE_EXTRA ("numspaces: %d", numspaces);
}

static void gdf_text (struct ui_out *uiout, char *string)
{
  GDF_TRACE_EXTRA ("string: %s", string);
}

static void gdf_message (struct ui_out *uiout, int verbosity, char *format,
                         va_list args)
{
  GDF_TRACE_EXTRA ("verbosity: %d", verbosity);
  vfprintf (stderr, format, args);
}

static void gdf_wrap_hint (struct ui_out *uiout, char *identstring)
{
  GDF_TRACE_EXTRA ("%s", identstring);
}

static void gdf_flush (struct ui_out *uiout)
{
  GDF_TRACE ();
}

struct ui_out *
gdf_out_new (GdfGdbCommander *cmdr)
{
  struct ui_out_data *data = g_new0 (struct ui_out_data, 1);
  
  data->cmdr = cmdr;
  return ui_out_new (&gdf_ui_out_impl, data, ui_source_list);
}

