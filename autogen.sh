#!/bin/sh
# Run this to generate all the initial makefiles, etc.

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

PKG_NAME="gdf-gdb"

OLDACLOCAL_FLAGS=$ACLOCAL_FLAGS
ACLOCAL_FLAGS="$ACLOCAL_FLAGS -I $srcdir/src/gdb/sim/common"

(test -f $srcdir/configure.in \
  && test -f $srcdir/src/commander/main.c) || {
    echo -n "**Error**: Directory "\`$srcdir\'" does not look like the"
    echo " top-level $PKG_NAME directory"
    exit 1
}

(test -f $srcdir/src/gdb/configure) || {
    echo "**Error**: GDB source not found.  Please read the HACKING file"
    exit 1
}

. $srcdir/macros/autogen.sh