#!/bin/sh

# Obtains a gdb snapshot from sourceware cvs

if test ! -f configure.in || test ! -d src/commander; then
    echo \*\*\* ERROR \*\*\* You are not in the GDF source tree!
    exit 1
fi

cd src
if test -d gdb; then
    echo Updating gdb...
    cd gdb;
    cvs -z9 up
    echo Sucess!
else
    echo Retrieving gdb...
    pass=`grep \:pserver\:anoncvs\@anoncvs.cygnus.com:\/cvs\/src ~/.cvspass`
    if test -z "$pass"; then
	echo "The password is 'anoncvs'."
	cvs -z9 -d :pserver:anoncvs@anoncvs.cygnus.com:/cvs/src login
    fi
    cvs -z9 -d :pserver:anoncvs@anoncvs.cygnus.com:/cvs/src co gdb

    echo Preparing gdb tree for use in GDF...
    if test -d src; then
	mv src gdb
    else
	echo \*\*\*ERROR\*\*\* Could not get gdb source tree.
    fi

    if test -f gdb/configure.in; then

	# Place a NO-AUTO-GEN file in each gdb subdirectory
	for gdb_subdir in `find ./gdb/ -print`
	do
	    if test -d $gdb_subdir; then
		touch $gdb_subdir/NO-AUTO-GEN
	    fi
	done
	echo Success!.
    else
	rm -rf gdb
	echo \*\*\*ERROR\*\*\* Could not get gdb source tree.
    fi
fi
